<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function Home() {
        return view('layouts.home');
    }
    public function BlogIndex() {
        return view('layouts.blog.index');
    }
    public function LoginRegister() {
        return view('layouts.account.index');
    }
}
