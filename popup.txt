 // Count time calling
    const measure = 0
    const ammount = 0
    const timer = $('#timer')
    const s = $(timer).find('.seconds')
    const m = $(timer).find('.minutes')
    const h = $(timer).find('.hours')

    var seconds = 0
    var minutes = 0
    var hours = 0

    var interval = null;

    $('#sales').on('click', function(){
        cronometer()
        if ($(ammount).length > -1) {
            startClock()
        }
    })
    $('#support').on('click', function(){
        cronometer()
        if ($(ammount).length > -1) {
            startClock()
        }
    })

    $('.btn-close').on('click', function() {
        restartClock()
    })

    $('button.btn-end-call').on('click', function() {
        restartClock()
    })

    function pad(d) {
        return (d < 10) ? '0' + d.toString() : d.toString()
    }

    function startClock() {
        hasStarted = false
        hasEnded = false

        seconds = 0
        minutes = 0
        hours = 0

        switch ($(measure).length) {
            case 's':
                if ($(ammount).length > 3599) {
                    let hou = Math.floor($(ammount).length / 3600)
                    hours = hou
                    let min = Math.floor(($(ammount).length - (hou * 3600)) / 60)
                    minutes = min;
                    let sec = ($(ammount).length - (hou * 3600)) - (min * 60)
                    seconds = sec
                }
                else if ($(ammount).length > 59) {
                    let min = Math.floor($(ammount).length / 60)
                    minutes = min
                    let sec = $(ammount).length - (min * 60)
                    seconds = sec
                }
                else {
                    seconds = $(ammount).length
                }
                break
            case 'm':
                if ($(ammount).length > 59) {
                    let hou = Math.floor($(ammount).length / 60)
                    hours = hou
                    let min = $(ammount).length - (hou * 60)
                    minutes = min
                }
                else {
                    minutes = $(ammount).length
                }
                break
            case 'h':
                hours = $(ammount).length
                break
            default:
                break
        }

        refreshClock()

        setTimeout(function(){
            $('#timer').fadeIn(350)
            $('#stop-timer').fadeIn(350)
        }, 350)
        }

    function restartClock() {
        clear(interval)
        hasStarted = false
        hasEnded = false

        seconds = 0
        minutes = 0
        hours = 0

        $(s).text('00')
        $(m).text('00')
        $(h).text('00')
    }

    var hasStarted = false
    var hasEnded = false
    if (hours == 0 && minutes == 0 && seconds == 0 && hasStarted == true) {
        hasEnded = true
    }

    function cronometer() {
        hasStarted = true
        interval = setInterval(() => {
            if (seconds < 59) {
                seconds++
                refreshClock()
            }
            else if (seconds == 59) {
                minutes++
                seconds = 0
                refreshClock()
            }
            if (minutes == 60) {
                hours++
                minutes = 0
                seconds = 0
                refreshClock()
            }
        }, 1000)
    }

    function refreshClock() {
        $(s).text(pad(seconds))
        $(m).text(pad(minutes))
        if (hours < 0) {
            $(s).text('00')
            $(m).text('00')
            $(h).text('00')
        } else {
            $(h).text(pad(hours))
        }

        if (hours == 0 && minutes == 0 && seconds == 0 && hasStarted == true) {
            hasEnded = true
            alert('The Timer has Ended !')
        }
    }

    function clear(intervalID) {
        clearInterval(intervalID)
        console.log('cleared the interval called ' + intervalID)
    }
});