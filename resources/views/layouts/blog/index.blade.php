@extends('layouts.master')
@section('content')
<div class="box-search-content search_active block-bg close__top">
    <form id="search_mini_form" class="minisearch" action="#">
        <div class="field__search">
            <input type="text" placeholder="Search entire store here...">
            <div class="action">
                <a href="#"><i class="zmdi zmdi-search"></i></a>
            </div>
        </div>
    </form>
    <div class="close__wrap">
        <span>close</span>
    </div>
</div>
<div class="ht__bradcaump__area bg-image--4">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="bradcaump__inner text-center">
                    <h2 class="bradcaump-title">Blog Page</h2>
                    <nav class="bradcaump-content">
                        <a class="breadcrumb_item" href="index.html">Home</a>
                        <span class="brd-separetor">/</span>
                        <span class="breadcrumb_item active">Blog</span>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="page-blog bg--white section-padding--lg blog-sidebar right-sidebar">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-12">
                <div class="blog-page">
                    <div class="page__header">
                        <h2>Tin tức mới</h2>
                    </div>
                    <!-- Start Single Post -->
                    <article class="blog__post d-flex flex-wrap">
                        <div class="thumb">
                            <a href="blog-details.html">
                                <img src="images/blog/blog-3/1.jpg" alt="blog images">
                            </a>
                        </div>
                        <div class="content">
                            <h4><a href="blog-details.html">Blog image post</a></h4>
                            <ul class="post__meta">
                                <li>Posts by : <a href="#">road theme</a></li>
                                <li class="post_separator">/</li>
                                <li>Mar 10 2018</li>
                            </ul>
                            <p>Donec vitae hendrerit arcu, sit amet faucibus nisl. Crastoup pretium arcu ex.
                                Aenean posuere libero eu augue rhoncus Praesent ornare tortor amet.</p>
                            <div class="blog__btn">
                                <a href="blog-details.html">read more</a>
                            </div>
                        </div>
                    </article>
                    <!-- End Single Post -->
                
                </div>
                <ul class="wn__pagination">
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#"><i class="zmdi zmdi-chevron-right"></i></a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-12 md-mt-40 sm-mt-40">
                <div class="wn__sidebar">
                    <!-- Start Single Widget -->
                    <aside class="widget search_widget">
                        <h3 class="widget-title">Tìm kiếm</h3>
                        <form action="#">
                            <div class="form-input">
                                <input type="text" placeholder="Search...">
                                <button><i class="fa fa-search"></i></button>
                            </div>
                        </form>
                    </aside>
                    <!-- End Single Widget -->
                    <!-- Start Single Widget -->
                    <aside class="widget recent_widget">
                        <h3 class="widget-title">Top sách yêu thích</h3>
                        <div class="recent-posts">
                            <ul>
                                <li>
                                    <div class="post-wrapper d-flex">
                                        <div class="thumb">
                                            <a href="blog-details.html"><img src="images/blog/sm-img/1.jpg" alt="blog images"></a>
                                        </div>
                                        <div class="content">
                                            <h4><a href="blog-details.html">Blog image post</a></h4>
                                            <p> March 10, 2015</p>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="post-wrapper d-flex">
                                        <div class="thumb">
                                            <a href="blog-details.html"><img src="images/blog/sm-img/2.jpg" alt="blog images"></a>
                                        </div>
                                        <div class="content">
                                            <h4><a href="blog-details.html">Post with Gallery</a></h4>
                                            <p> March 10, 2015</p>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="post-wrapper d-flex">
                                        <div class="thumb">
                                            <a href="blog-details.html"><img src="images/blog/sm-img/3.jpg" alt="blog images"></a>
                                        </div>
                                        <div class="content">
                                            <h4><a href="blog-details.html">Post with Video</a></h4>
                                            <p> March 10, 2015</p>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="post-wrapper d-flex">
                                        <div class="thumb">
                                            <a href="blog-details.html"><img src="images/blog/sm-img/4.jpg" alt="blog images"></a>
                                        </div>
                                        <div class="content">
                                            <h4><a href="blog-details.html">Maecenas ultricies</a></h4>
                                            <p> March 10, 2015</p>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </aside>
                    <!-- End Single Widget -->
                    <!-- Start Single Widget -->
                    <aside class="widget comment_widget">
                        <h3 class="widget-title">Bình luận mới</h3>
                        <ul>
                            <li>
                                <div class="post-wrapper">
                                    <div class="thumb">
                                        <img src="images/blog/comment/1.jpg" alt="Comment images">
                                    </div>
                                    <div class="content">
                                        <p>demo says:</p>
                                        <a href="#">Quisque semper nunc vitae...</a>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="post-wrapper">
                                    <div class="thumb">
                                        <img src="images/blog/comment/1.jpg" alt="Comment images">
                                    </div>
                                    <div class="content">
                                        <p>Admin says:</p>
                                        <a href="#">Curabitur aliquet pulvinar...</a>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="post-wrapper">
                                    <div class="thumb">
                                        <img src="images/blog/comment/1.jpg" alt="Comment images">
                                    </div>
                                    <div class="content">
                                        <p>Irin says:</p>
                                        <a href="#">Quisque semper nunc vitae...</a>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="post-wrapper">
                                    <div class="thumb">
                                        <img src="images/blog/comment/1.jpg" alt="Comment images">
                                    </div>
                                    <div class="content">
                                        <p>Boighor says:</p>
                                        <a href="#">Quisque semper nunc vitae...</a>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="post-wrapper">
                                    <div class="thumb">
                                        <img src="images/blog/comment/1.jpg" alt="Comment images">
                                    </div>
                                    <div class="content">
                                        <p>demo says:</p>
                                        <a href="#">Quisque semper nunc vitae...</a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </aside>
                    <!-- End Single Widget -->
                    <!-- Start Single Widget -->
                    <aside class="widget category_widget">
                        <h3 class="widget-title">Danh mục</h3>
                        <ul>
                            <li><a href="#">Fashion</a></li>
                            <li><a href="#">Creative</a></li>
                            <li><a href="#">Electronics</a></li>
                            <li><a href="#">Kids</a></li>
                            <li><a href="#">Flower</a></li>
                            <li><a href="#">Books</a></li>
                            <li><a href="#">Jewelle</a></li>
                        </ul>
                    </aside>
                    <!-- End Single Widget -->
            
                </div>
            </div>
        </div>
    </div>
</div>
@endsection