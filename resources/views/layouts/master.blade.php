<!doctype html>
<html class="no-js" lang="zxx">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Review Sách</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Favicons -->
	<link rel="shortcut icon" href="images/favicon.ico">
	<link rel="apple-touch-icon" href="images/icon.png">

	<!-- Google font (font-family: 'Roboto', sans-serif; Poppins ; Satisfy) -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,600,600i,700,700i,800"
		rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">

	<!-- Stylesheets -->
<link rel="stylesheet" href="{{asset('public/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{asset('public/css/plugins.css')}}">
	<link rel="stylesheet" href="{{asset('public/css/style.css')}}">

	<!-- Cusom css -->
	<link rel="stylesheet" href="{{asset('public/css/custom.css')}}">

	<!-- Modernizer js -->
	<script src="{{asset('public/js/vendor/modernizr-3.5.0.min.js')}}"></script>
</head>

<body>
    {{-- Header --}}
	@include('layouts.header')

    {{-- Main --}}
    <div class="wrapper" id="wrapper">
	@yield('content')
    </div>
	{{-- Footer --}}
    @include('layouts.footer')
    
 	<!-- JS Files -->
     <script src="{{asset('public/js/vendor/jquery-3.2.1.min.js')}}"></script>
     <script src="{{asset('public/js/popper.min.js')}}"></script>
     <script src="{{asset('public/js/bootstrap.min.js')}}"></script>
     <script src="{{asset('public/js/plugins.js')}}"></script>
     <script src="{{asset('public/js/active.js')}}"></script>
 
 </body>
 </html>